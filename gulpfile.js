var gulp = require('gulp'),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    connect = require('gulp-connect');

gulp.task('sass', function() {
    gulp.src('./src/styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./build/assets'))
        .pipe(connect.reload());
});

gulp.task('pug', function() {
    gulp.src('./src/templates/examples/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('./build/'))
        .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch('./src/styles/**/*.scss', ['sass']);
    gulp.watch('./src/templates/**/*.pug', ['pug']);
});

gulp.task('dev:serv', ['sass', 'pug', 'watch'], function() {
    connect.server({
        root: './build',
        livereload: true
    });
});