
var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    pug         = require('gulp-pug'),
    connect     = require('gulp-connect'),
    sequence    = require('gulp-sequence');


/**
 * compile scss to css
 */
gulp.task('scss', function(cb) {
    gulp.src('./src/styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./build/assets/'))
        .pipe(connect.reload());

    cb();
    return null;
});


/**
 * compile pug to html
 */
gulp.task('pug', function(cb) {
    gulp.src('./src/templates/examples/*.pug')
    .pipe(pug({}))
    .pipe(gulp.dest('./build/'))
    .pipe(connect.reload());

    cb();
    return null;
});

/**
 * watch changes
 */
gulp.task('watch', function(cb) {

    // scss
    gulp.watch('./src/styles/**/*.scss', ['scss']);

    // pug
    gulp.watch('./src/templates/**/*.pug', ['pug']);

    cb();

});


/**
 * start server
 */
gulp.task('server', function(cb) {
    connect.server({
        root: './build',
        livereload: true
    });

    cb();

});


/**
 * start devel
 */
gulp.task('start:dev', sequence(['scss', 'pug'], 'watch', 'server'));